//
// Created by max on 11/04/2022.
//

#ifndef VOXBLOX_TEX_VOXEL_H
#define VOXBLOX_TEX_VOXEL_H

#include <cstdint>
#include <string>
#include "voxblox/core/color.h"
#include "voxblox/core/common.h"
#include "voxblox/core/voxel.h"

#define TEXEL_WIDTH = 4


namespace voxblox {

    /**
     * Direction for the 2D texture plane inside the 3D voxel.
     * Each axis defines the texture plane's normal direction.
     */
    enum TexDir : uint8_t {
        X = 0,
        Y = 1,
        Z = 2
    };

    // TODO: look into alignment for these struct
    /**
     * 2D texture point on a plane inside the corresponding 3D voxel
     */
    struct Texel {
        uint8_t color_prev[3] = {};
        uint8_t color[3] = {};
        // weight used for blending with new color values
        uint8_t weight = 0;
        float depth = 0.0f;
    };

    /**
     * Voxel in 3D space with associated 2D texture plane
     */
    struct TexVoxel {
        float distance = 0.0f;
        float weight = 0.0f;
        // 2D texture plane in Voxel
        TexDir texDir = TexDir::X;
        // TODO: make texture size parametric
        Texel *texels;
    };

    namespace voxel_types {
        const std::string kTexVoxel = "tex_voxel";
    } // namespace voxel_types

    template<>
    inline std::string getVoxelType<TexVoxel>() {
        return voxel_types::kTexVoxel;
    }


}// namespace voxblox
#endif //VOXBLOX_TEX_VOXEL_H
