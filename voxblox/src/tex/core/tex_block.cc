//
// Created by max on 11/04/2022.
//

#include "voxblox/core/block.h"
#include "voxblox/core/voxel.h"
#include "voxblox/tex/core/tex_voxel.h"

namespace voxblox {
    // Deserialization
    template <>
    void Block<TexVoxel>::deserializeFromIntegers(const std::vector<uint32_t>& data) {
        constexpr size_t kNumDataPacketsPerVoxel = 3u;
        const size_t num_data_packets = data.size();
        CHECK_EQ(num_voxels_ * kNumDataPacketsPerVoxel, num_data_packets);
        for (size_t voxel_idx = 0u, data_idx = 0u;
             voxel_idx < num_voxels_ && data_idx < num_data_packets;
             ++voxel_idx, data_idx += kNumDataPacketsPerVoxel) {
            const uint32_t bytes_1 = data[data_idx];
            const uint32_t bytes_2 = data[data_idx + 1u];
            const uint32_t bytes_3 = data[data_idx + 2u];

            TexVoxel& voxel = voxels_[voxel_idx];

            // TODO(mfehr, helenol): find a better way to do this!
            memcpy(&(voxel.distance), &bytes_1, sizeof(bytes_1));
            memcpy(&(voxel.weight), &bytes_2, sizeof(bytes_2));

//            voxel.color.r = static_cast<uint8_t>(bytes_3 >> 24);
//            voxel.color.g = static_cast<uint8_t>((bytes_3 & 0x00FF0000) >> 16);
//            voxel.color.b = static_cast<uint8_t>((bytes_3 & 0x0000FF00) >> 8);
//            voxel.color.a = static_cast<uint8_t>(bytes_3 & 0x000000FF);
        }
    }

    // Serialization
    template <>
    void Block<TexVoxel>::serializeToIntegers(std::vector<uint32_t>* data) const {
        CHECK_NOTNULL(data);
        constexpr size_t kNumDataPacketsPerVoxel = 3u;
        data->clear();
        data->reserve(num_voxels_ * kNumDataPacketsPerVoxel);
        for (size_t voxel_idx = 0u; voxel_idx < num_voxels_; ++voxel_idx) {
            const TexVoxel& voxel = voxels_[voxel_idx];
            // pack voxel bytes in to uint32 vector
            // TODO(mfehr, helenol): find a better way to do this!
            const uint32_t* bytes_1_ptr =
                    reinterpret_cast<const uint32_t*>(&voxel.distance);
            data->push_back(*bytes_1_ptr);

            const uint32_t* bytes_2_ptr =
                    reinterpret_cast<const uint32_t*>(&voxel.weight);
            data->push_back(*bytes_2_ptr);

//            data->push_back(static_cast<uint32_t>(voxel.color.a) |
//                            (static_cast<uint32_t>(voxel.color.b) << 8) |
//                            (static_cast<uint32_t>(voxel.color.g) << 16) |
//                            (static_cast<uint32_t>(voxel.color.r) << 24));
        }
        CHECK_EQ(num_voxels_ * kNumDataPacketsPerVoxel, data->size());
    }

} // namespace voxblox
