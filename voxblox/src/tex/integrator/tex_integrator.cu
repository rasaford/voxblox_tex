//
// Created by max on 12/04/2022.
//
#include<stdio.h>
//#include <helper_cuda.h>
//#include <helper_math.h>
#include<cuda_runtime_api.h>

__global__ void vecAddKernel(const float *a, const float *b, float *out) {
    unsigned int i = threadIdx.x;
    out[i] = a[i] + b[i];
}

extern "C" void vecAdd(const size_t size, const float *a, const float *b, float *out) {
    vecAddKernel<<<1, size>>>(a, b, out);
}

